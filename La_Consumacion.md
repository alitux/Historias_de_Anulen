## La consumación

Un extraterrestre que camina como humano, que come como humano, que besa como humano, que envejece como humano, deambula en la tierra. Los que lo conocieron lo describían como alguien normal y afirmaban sin miedo a equivocarse que: caminaba como humano, comía como humano, etc... 

Un día cualquiera del año 2718 cerró su ciclo de recolectar información y supo que tenía que irse de la tierra. Calentó el agua y se tomó los últimos mates mirando el horizonte, la consumación iba a ocurrir puntualmente a a las 08:33 AM y cuando menos lo esperara estaría enjaulado en la rutina de Ross 128 b. Un destello multicolor iluminó el puerto de Wulaia, sabía que debía despedirse. La mañana se aclaró en un instante y volvió a su fría desolación invernal. El mate se enfrió sobre la mesa. Perdida en alguna roca del paso Murray hay una placa que lo recuerda: "Aquí se consumó Orundellico. El que fue distinto e igual todos."

**Alitux (9 de Agosto de 2018)**
