## El señor Escondido

Había un señor que vivía metido en una bolsa
que estaba dentro de una caja
que habían puesto debajo de una mesa.
Eso estaba en una habitación
que tenía la puerta cerrada con llaves
y candados.

Era la habitación más escondida de una casa grande,
estaba en el patio y jamás se hubiera esperado encontrar
un cuarto ahí
porque habían dejado crecer muchas plantas encima.
El jardín estaba rodeado por muros tan altos
que nadie pensaba en saltarlos sino, tan sólo,
en lo altos que eran.
De manera que la única forma de llegar al jardín
era entrar por la casa.
Cuando uno cruzaba esa puerta, encontraba
tantos pasillos y habitaciones
que cualquiera se hubiera perdido un buen rato
antes de llegar al jardín.
Metido en esa bolsa
que estaba dentro de la caja
había un señor muerto de miedo.
Era un señor que había viajado mucho
por todo el mundo,
tan ancho y largo como es,
viviendo grandes aventuras,
y en todas había sido valiente.
Sin embargo un día regresó, quién sabe de dónde,
y pidió una bolsa
y una caja, cerraduras, candados y ordenó
que construyeran la habitación del patio
y que dejaran crecer plantas sobre ella.
Pidió que una señora le llevara comida
para no tener que salir nunca.
Y allí se escondió para siempre…
o para casi siempre.

Aun cuando yo el que escribe este cuento, no
se me ocurre qué fue lo que pasó, por qué volvió tan
asustado. No lo sé y temo que tardaría demasiado en
descubrirlo. En cambio, hay varias posibilidades sobre
cómo salió:

1) A pesar de estar tan escondido oía los ruidos
de la lluvia, el canto de los pájaros y el ladrido de su
perro y se preguntó: ¿quién les dará agua?, ¿quién les
dará de comer y quién cuidará las plantas? Volver a ver
todo eso fue más fuerte y salió.

2) Pensé que la casa se podía incendiar obligándolo
a salir corriendo para salvarse. Pero eso no me
gustó porque lo haría vivir con más miedo, y porque
era una hermosa casa.

3) Alguien, que conocía sus aventuras, conseguía
entrar a pedirle ayuda, no una gran ayuda, porque
nunca se hubiera atrevido, una pequeña ayuda
que él podía dar.

4) Un escritor se entera de su historia y quiere
escribir un libro. Él siente vergüenza de atenderlo metido
en su bolsa y lo recibe todas las tardes, tomando
un té en la sala. Así se acostumbra de nuevo a estar
afuera.

5) La mujer que le lleva la comida no es una señora
grande o vieja. Es una mujer joven y buena. A él
le llama la atención que su voz sea tan suave y que
jamás le haya pedido que salga de la bolsa, que jamás
lo haya querido convencer. Nunca sintió ni un reproche
ni una burla en la voz de esa mujer. Un día quiere verle
la cara y asoma su cabeza; ella era tan tímida como
él y pasa mucho tiempo hasta que se hablan más allá
de los saludos. Pasa otro tiempo hasta que se hacen
grandes amigos y él empieza a sentirse un poco incómodo
de recibirla metido en una bolsa. Y pasa más
tiempo y pasan otras cosas.

** Luis Pescetti - El pulpo está crudo - **
