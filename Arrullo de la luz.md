Duérmase sabia luz
por hoy no alumbre mas
que del otro lado
una flor la quiere igual.

Y si llega al infinito
cuentele de nuestra flor
que se llena de colores
arrullada por el sol.

**Alitux(Algún momento del año 2016)**
