## Historia del Velo de Novia

Cuenta la leyenda que Karyen, una joven Ona, se había enamorado de Yonolpe. Para poder vivir con él debía esperar pasar la prueba del Hain, ritual en que los niños pasaban unas pruebas de destreza y valor que los convertiría en adultos.

La prueba mayor era cazar al guanaco blanco, quién lograra hacerlo podría elegir primero a su esposa. Karyen esperaba que Yonolpe fuera el primero en llegar y pedirle casamiento pero no fue así, otro Kloketen (o iniciado) llegó con la piel del guanaco blanco y pidió la mano de la Ona; la joven lo rechazó y corrió al bosque buscando a su amado, el Kloketen enfurecido fue en busca del otro joven iniciado y le dio un golpe de muerte.

Karyen lloró tanto que murió en el regazo de Yonolpe. Kra la luna tuvo pena de ella.

Cuentan los viejos de la tribu que Kra la luna convirtió a Yonolpe en un gran monte y a la Ona en un cascada que nace en la cima, ellos son el Monte Olivia y la cascada del Velo de la Novia, así la luna dejó un mensaje a todos los habitantes de la tierra, nadie puede intervenir en el amor.

Desde aquellos días tanto hombres como mujeres van a tomar el agua del Velo de la Novia y a pedir a Kra la luna que afiance su amor para siempre...

**(Anónimo)**