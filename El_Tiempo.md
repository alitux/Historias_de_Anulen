## El Tiempo (Eduardo Galeano)

El tiempo de los mayas nació y tuvo nombre cuando no existía el cielo había despertado todavía la tierra.

Los días partieron del oriente y se echaron a caminar.

El primer día sacó de sus entrañas al cielo y a la tierra.

El segundo día hizo la escalera por donde baja la lluvia.

Obras del tercero fueron los ciclos de la mar y de la tierra y la muchedumbre de las cosas.

Por voluntad del cuarto día, la tierra y el cielo se inclinaron y pudieron encontrarse.

El quinto día decidió que todos trabajaran.

Del sexto salió la primera luz.

En los lugares donde no había nada, el séptimo día puso tierra. El octavo clavó en la tierra sus manos y sus pies.

El noveno día creó los mundos inferiores. El décimo día destinó los mundos inferiores a quienes tienen veneno en el alma.

Dentro del sol, el undécimo día modeló la piedra y el árbol.

Fue el duodécimo quien hizo el viento. Sopló viento y lo llamó espíritu, porque no había muerte dentro de él.

El decimotercer día mojó la tierra y con barro asomó un cuerpo como el nuestro.

Así se recuerda en Yucatán.

**Eduardo Galeano, Mitos de Memoria del fuego. Ilustraciones de Elisa Arguilé. Anaya, 2002**